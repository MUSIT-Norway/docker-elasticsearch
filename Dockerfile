FROM docker.elastic.co/elasticsearch/elasticsearch:5.6.2

ENV xpack.security.enabled=false
ENV cluster.name=musit-cluster
ENV ES_JAVA_OPTS="-Xms512m -Xmx512m"

EXPOSE 9200 9300

CMD ["/bin/bash", "bin/es-docker"]

